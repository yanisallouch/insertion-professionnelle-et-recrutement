# Entretien de recrutement

Un recrutement peut se décomposer de la façon suivante :

1. [Entretien téléphonique (RH) (~20min)](#entretienTelephonique);
2. [Entretien technique (RH + référent technique) (~60min)](#entretienTechnique);
3. Test technique (optionnelle) (~30min);
4. Test de langue (optionnelle) (~30min);
5. Retour d'évaluation (optionnelle).


## <a name="entretienTelephonique"></a> La qualification téléphonique (~20min)

C'est la première étape de sélection laissé a la responsabilité des RH.

> Objectif : Convaincre entre **10 à 30 min** maximum.

Avoir sous les yeux son CV, relevés de notes, offres d'emplois,...

* Etre clair et synthétique;
* Proposer un nouveau créneau si occupé ou ne pas décrocher si le moment ne s'y prête pas;
* **Bannir** le `oui` et le `non`, *il faut argumentez*!
* Attention à sa posture et sa communication, même au téléphone !
* Soigner la fin de l'entretien.

Se préparer ne veut pas dire réciter !

### Presentation rapide (~5min)

Une présentation synthétique pourrait être organiser comme ceci :

1. Se présenter : poser le cadre, le contexte, les bases en moins de 1min;
2. Les circonstances de votre candidature : expliquer pourquoi vous êtes ici en moins de 1 min;
3. Votre motivation : étayer votre motivation en moins de 2 min.


#### Se présenter (<1min)

* Votre identité : Nom, prénom;
* Votre situation : jeune diplômé, stagiaire, en poste, en recherche active, en veille active, disponible, soumis à un préavis ...
* Votre parcours : diplômes, formations, expériences (les 3 plus significatives), privilégiez la chronologie, restez focalisés sur les éléments essentiels des missions (ne pas rentrez dans les détails sans valeur ajoutée).

#### Ce que vous souhaitez faire / votre recherche (<1min)

* Faire le rapprochement entre expériences et le poste;
* Mettre en valeur les éléments du poste proposé avec vos souhaits et perspectives;
* Amenez la conversation vers quelques chose comme : 
	* Ceci a confirmé ma volonté de ...
	* Toutes ces expériences m'ont donné les compétences de ...

#### Pourquoi avoir choisi cette entreprise (<1min)?

* Expliquer pourquoi cette entreprise ?
* Mettre en avant les compétences / expériences utiles pour eux;
* Mettre en avant vos qualités qui vous démarqueront.


### Réponse aux Questions

* Pourquoi vous ?
* Que connaissez vous de notre entreprise ?
* Pourquoi souhaitez vous nous rejoindre ?
* Quels sont vos critères de choix ?
* Vos prétentions ?
* Votre mobilité géographique ?

### Questions a poser

* Sur l'entreprise, son fonctionnement ?
* Sur le processus de recrutement, les critères de sélection ?
* Sur le poste proposé, les missions, l'équipe ?

## <a name="entretienTechnique"></a> Entretien technique (embauche)

Pour **l'employeur** :
> Objectif : vérifier l'adéquation entre les compétences du candidat et ses besoins pour le poste à pourvoir.

Pour **le candidat** :
> Objectif : Convaincre

Pour mieux se connaitre avant l'entretien, des tests peuvent vous aider :
* "Les préférences cérébrales" Ned Hermann;
* Le test HBDI;
* DISC ...etc.

> **Ne pas hésiter à demander** au recruteur si des tests seront réalisés lors de la venue en entretiens et s’y préparer !

Ces entretien reste avant tout un jeu de questions/réponses entre deux parties (ou plusieurs), où votre capacité a argumenter (réthorique) et soutenir vos intérêts sont évalués.

### Quelques détails à surveiller avant son arrivée (même en visio)

* **Optez pour** une tenue "professionnelle", adaptée a la Socièté que vous allez rencontrer.
Exemple :
	* Si c'est un **grand groupe** : chemise, cravate...
	* Si c'est une **startup** : style plutôt jeune, pantalon et une chemise.

### Quelques détails à surveiller avant l'entretien

**Être à l'heure !** 

La ponctualité est importante en milieu professionnel...

Conseil :
> Arriver 5 à 10 minutes à l'avance. 
> Eteindre le téléphone, notifications, alarmes...
> Demandez un verre d'eau !
> Donnez une bonne **première** impression...
> Soigner sa poignée de main !


### Qu'est ce qui est observer en entretien physique ?

On peux distinguer deux types **non proportionnelles** :

1. Sur **la forme** (90%) :
	1. Le non-verbal : posture générale du corps, les gestes, les mouvements (60%);
	2. Le para-verbal : le ton, la voix, le regard, le débit (40%);
2. Sur **le fond** (10%) :
	1. Le verbal : le contenu de vos argumentations.

Soit presque toute la communication est faite non-verbalement.

### Qu'est ce que l'entreprise attend de vous ?

**Attention** :

> Le niveau d'exigence de l'entreprises varie en fonction de l'expérience et du poste visé.

* Qualité de l'expression orale et de la présentation;
* Compétences techniques;
* Compétences comportementales, posture et personnalité;
* Intérêt pour le poste de l'entreprise;
* Projection dans la durée;
* Motivations et objectifs professionnels.

### En général

* Soyez avenant, souriant !
* Soignez la posture, sa gestuelle;
* Parlez, mais **écouter**. Prenez des notes et posez des questions.
* **Le vouvoiemment est de riguer**; sauf si le recruteur vous propose de le tutoiement.
* Bannir l'arrogance, se valoriser en toute modestie;
* Evitez les tics de langage (euh, en fante, moi je, donc...), ça dénote un manque de préparation et de profesionnalisme.
* Regardez dans les yeux;
* Parlez avec vos mains si cela vous aide, sans en faire beaucoup.
* **Posez vos mains sur la table si vous sentez qu'elle peuvent trembler** !
* Décroisez les jambes et posez vos pieds à plat sur le sol !

### Les techniques

> Se renseigner et s'adapter en fonction du poste du recruteur

C'est à dire qu'il faut adapter ses réponses et ses questions en fonction de l'interlocuteur.

Quand l'interlocteur vous demande :
-  "Parlez moi de vous ..." :
 	- Vous répondez par un discours **SYNTHETIQUE**
- "Pouvez vous me parler d'une expérience professionnelle dont vous êtes particulièrement fièr(e) ?" :
	- Vous choississez une expérience de préférence en lien avec le poste visé !
	- Décrire le contexte, les enjeux, les actions menées, les résultats obtenus.
- "Quels sont vos objectifs professionnels sur du long terme ? :
	- Il faut répondre avec ses envies d'évolutions en restant réaliste et non pas farfelues (le RH étudiera votre réponse pour la suite de votre carrière) !
- "Pouquoi notre entreprise vous intéresse ?" :
	- Soyez informez des activités de l'entreprise avant le rendez-vous, et préparer quelques réponses.
- "Avez vous déjà rencontré des problèmes / surmonté des périodes de stress, quelles sont les solutions que vous avez mis en oeuvre pour y remédier ?" :
	- **Ne surtout pas se dévalorisez**, l'important c'est de réagir.
- "Quels sont vos points faibles ? Forts ?" :
	- Donnez un défaut mais aussi les actions que vous entreprenez pour le corriger.
	- Demander à ses amis / son entourage en amont pour avoir des exemples.
- "Quels sont vos prétentions salariales ?" :
	- Il est préférable d'attendre que l'on vous pose la question, mais il faut bien réfléchir en amont à la fourchette salariale souhaitée.
- "Quels sont vos loisirs ?" :
	- **Ne pas détailler** des passions **trop intimes** ou **difficile** à expliquer.
	- Donner des exemples concrets et pourquoi vous avez mis en avant ce/ces loisir(s).

### Questions par les recruteurs

Récapitulons les questions auquelles vous pouvez être confontrées.

#### Questions Types

* Quelle expérience vous a le plus marqué ? Pourquoi ?
* Qu'avez vous tiré de cette expérience ?
* Pourquoi êtes vous à l’écoute du marché ? Pourquoi ce secteur d’activité ?
* Qu’est ce que votre ancienne entreprise vous a apporté ? Qu’est ce qui vous manque aujourd’hui ?
* Qu’est ce que vous souhaitez trouver à tout prix dans votre futur poste ?
* Pourquoi ce poste ? Quel poste pourrait vous attirer dans un futur proche ? Quel projet professionnel sur les 3 puis 10 ans à venir ?
* Qu’est ce qu’un bon manager selon vous ? Quelles seraient les qualités que vous aimeriez que votre manager possède?
* Comment votre précédent manager vous décrirait ? qu’est ce qu’il dirait au sujet de votre personnalité ? de votre travail ?
* Qu’est ce qui vous anime au quotidien ? De quoi êtes vous le plus fier ?
* Définitions : notion d’engagement / d’exigence / le sens du service client : à quoi cela vous fait penser ? c’est quoi pour vous ?
* Quelle motivation et quel objectif professionnel souhaitez vous ?
* Quel intérêt pour notre société plus spécifiquement avez vous ?
* Quelles sont vos prétentions salariales ? Votre rémunération actuelle (fixe + variable) ?

#### Questions Atypiques

* Etes vous un leader ou un suiveur ?
* Avec quel type de personnalité aimez vous travailler ?
* Quelles entreprises vous passionnent ? Et pourquoi ?
* Décrivez moi la pièce (capacité à présenter les choses synthétiques/structurées) ?
* Parlez moi d’un de vos échecs ?
* C’est quoi un client pour vous ?
* Si vous étiez un animal, quel animal seriez vous ?
* Si vous gagnez 1 Million d'euros que faites vous ?

### Questions par les candidats

#### Sur le poste :

* Quelles sont les qualités nécessaires pour ce poste ?
* Quelles sont les compétences qui vous manquent dans l’équipe actuelle ?
* Quel est le plus gros challenge auquel la personne à ce poste sera confrontée ?
* S’il y en a un, avez vous une idée du budget qui me serait confié ?
* S'agit il d'une création de poste ? Comment est composée l‘équipe ?

#### Sur l'équipe :

* Pourriez vous m’en dire un peu plus sur l’équipe avec qui je serai amené à travailler ?
* Qui sera mon manager et quelles sont ses responsabilités ?

#### Sur la performance :

* Quelles sont les étapes les plus importantes du poste à franchir et à quelles échéances les attendez vous après la prise de poste ?
* Quels sont les objectifs à atteindre sur six mois / 1 an ?
* Comment se déroule le processus d’évaluation ?
* Quels sont les paramètres qui seront pris en compte pour mesurer ma performance/mon évolution ?

#### Sur l'entreprise :

* Je me suis renseigné sur la création de l’entreprise, mais pourriez vous m’en dire un peu plus sur… sur…? (cette question est valable uniquement si vous ne pouvez pas trouver cette information par vous même, sur le site de l’entreprise ou dans la presse) ?
* Quel est le dispositif d’accompagnement RH mis en place au sein de votre entreprise ? ?
* De manière globale, quels sont les objectifs actuels de l’entreprise/agence et en quoi mon équipe y contribue ?

#### Sur la culture d'entreprise :

* Comment décririez vous l’ambiance de travail ? Collaborative ou autonome ?
* Organisez vous des événements de "team building" ?

#### Avant de conclure :

* Quelles sont les prochaines étapes du processus de recrutement ?
* Est ce qu’il vous reste un point que vous souhaiteriez éclaircir sur mon parcours ou mes expériences professionnelles ?

### Les softs skills (se connaitre)

Réfléchir sur soi : c’est cultiver sa différence !

> Définir une image authentique et singulière de soi qui met en valeur vos talents.

Exemple : https://www.bloomr-impulse.com/blog/liste-46-soft-skills.
Autre exemple de test disponible ici : https://app.talentoday.com (freemium)

Définir vos caractéristiques dans chaque catégorie, par exemple :

* Communication;
* Esprit critique;
* Attitude constructive;
* Leadership;
* Ethique pro;
* Travail d'équipe.

### La fin de l'entretien

> En cas de réponse négative, si vous le souhaitez, vous pouvez demander un feedback afin de connaître les raisons et les pistes d’amélioration pour vos futurs entretiens !

* Demandez à la fin de l’entretien la durée du processus et l’échéance du retour;
* Envoyez un mail de remerciement en renouvelant votre motivation et votre intérêt pour le poste;
* Relancez **une fois le recruteur** afin de connaître la suite du processus (**attendre environ 15 jours**).


> Gardez le lien sur les réseaux sociaux (linkedin, ...) peut être un moyen de reprendre contact plus tard !
